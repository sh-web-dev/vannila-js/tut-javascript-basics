class Car {
  constructor( settings ){
  	this.type = 'car';
    this.wheels = settings.wheels;
  }
  drive(){
  	return 'I am driving';
  }
}

class Honda extends Car {
	constructor(settings){
  	super(settings);
    this.model = settings.model;
  }
}

const car = new Car({ wheels: 4 });
car.drive();
car.type;
car.wheels;
const accord = new Honda({
model: 'Honda',
  wheels: 4
});
accord.model;